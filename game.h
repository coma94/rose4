//////////
//  Copyright 2014-2015 Etienne Moutot <e.web@moutot.eu>
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

#ifndef H_GAME
#define H_GAME

#include <stdio.h>

typedef struct game game;
struct game
{
        int turn;
        int t[7][6];
};

/*
  convert char <-> int
  '_' = 0
  'X' = 1
  'O' = 2
*/
char to_char(int i);
int to_int(char c);

/*
  Decodes the game from an opened file (read access)
*/
game decode(FILE* file);

/*
  Write a game to an opened file (write access)
*/
void to_file(game g, FILE* file);

/*
  Prints the game on screen
*/
void print_game(game g);

/*
  Computes if we can play in col *col*. Returns -1 if not possible, return the line where to play if possible
*/
int play_allowed(game* g, int col);

/*
  plays in a column, return the line where played if normal, -1 if it is impossible to play
*/
int play(game* g, int col);

/*
  Undo a play. Same return as *play*
*/
int undo(game* g, int col);

#endif
