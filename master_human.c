//////////
//  Copyright 2014-2015 Etienne Moutot <e.web@moutot.eu>
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "game.h"
#include "eval.h"

int win(game* g)
{
        int col;
        int j;

        for(col=0; col<7; col++)
        {
                for(j=0; j<6; j++)
                {
                        if(g->t[col][j] != 0)
                        {
                                if(nb_align(g, g->t[col][j], col, j) == 4)
                                        return 1;
                        }
                }
        }

        return 0;
}

int main(int argc, char **argv)
{
        srand(time(0));

        system("./gen_game game.txt");
        
        while(1)
        {
                ////////////// load
                if(argc < 2)
                {
                        printf("usage: %s filename\n", argv[0]);
                        return 0;
                }

                game g;

                char filename[32];
                strcpy(filename, argv[1]);
                FILE *file = fopen(filename, "r");
        
                if(file)
                {
                        g = decode(file);

                        printf("turn: %d\n", g.turn);
                
                        int i; int j;
                        for(j=5;j>=0;j--)
                        {
                                for(i=0;i<7;i++)
                                {
                                        printf("%c ", to_char(g.t[i][j]));
                                }
                                printf("\n");
                        }
                        for(i=0;i<7;i++)
                        {
                                printf("--", i);
                        }
                        printf("\n");
                        for(i=0;i<7;i++)
                        {
                                printf("%d ", i);
                        }
                        printf("\n");
                
                        fclose(file);
                }else{
                        printf("Cannot open %s in r mode", filename);
                }

                // IA's turn loaded
                if(win(&g))
                {
                        printf("L'IA a gagné\n");
                        break;
                }
        
                //////////////
                ////////////// game

                int col;
                scanf("%d", &col);
        
                play(&g, col);
        
                //////////////
                ////////////// write
                file = fopen(filename, "w");
                if(file)
                {
                
                        int i; int j;
                        for(j=5;j>=0;j--)
                        {
                                for(i=0;i<7;i++)
                                {
                                        printf("%c ", to_char(g.t[i][j]));
                                }
                                printf("\n");
                        }

                        to_file(g, file);
                
                        fclose(file);
                }else{
                        printf("Cannot open %s in w mode", filename);
                }

                if(win(&g))
                {
                        printf("Le joueur a gagné\n");
                        break;
                }

                // test if player won

                system("./turn game.txt");
        }

	return 0;
}
