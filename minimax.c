//////////
//  Copyright 2014-2015 Etienne Moutot <e.web@moutot.eu>
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

#include "minimax.h"

#include <pthread.h>
#include <limits.h>

void* make_tree(void* args)
{
        // Get args
        make_tree_arg* a = (make_tree_arg*)args;
        tree** t = a->t;
        game g = a->g;
        int h0 = a->h0;
        int h = a->h;
        int main_player = a->main_player;

        // If main tree (h=h0), make threads.
        pthread_t th[7];
        make_tree_arg arg[7];
        
        int played[7];

        

        (*t) = (tree*)malloc(sizeof(tree));
        
        int i;
        for(i=0; i<7; i++)
        {
                (*t)->sons[i] = NULL;
        }

        if(h<=0) // Leaf of the tree
        {
                if(g.turn == main_player)
                        (*t)->value = eval(&g);
                else
                        (*t)->value = - eval(&g);
        }else{ // There are sons to make

                // If main player: maximize, else: minimize.
                if(g.turn == main_player)
                        (*t)->value = INT_MIN;
                else
                        (*t)->value = INT_MAX;

                int i;
                for(i=0; i<7; i++)
                {
                        game gi = g;
                        // If can't play in the column, don't bluid the subtree
                        //if(play(&gi, i) == -1)
                        //        continue;


                        played[i] = play(&gi,i);
                        
                        //Change player
                        gi.turn = 3 - gi.turn;

                        // Make sons
                        arg[i].t = &((*t)->sons[i]);
                        arg[i].g = gi;
                        arg[i].h0 = h0;
                        arg[i].h = h-1;
                        arg[i].main_player = main_player;

                        // If the play wins or looses, stop computing tree
                        if(nb_align(&g, g.turn, i, played[i]) == 4 && played[i] != -1)
                        {
                                if(g.turn == main_player)
                                        (*t)->value = _WIN_CST_STOP*10;
                                else
                                        (*t)->value = - _WIN_CST_STOP*10;

                                // Wait for launched threads to finish
                                if(h==h0)
                                {
                                int j;
                                for(j=0; j<i; j++)
                                {
                                        int ret = pthread_join(th[j], 0);
                                        if(ret > 0)
                                        {
                                                printf("An error occurred while joining0 thread %d, i=%d\n", j, i);
                                                exit(-1);
                                        }
                                }
                                }
                                
                                return;
                        }
                        
                        if(h==h0) // Multithread if it is main tree
                        {
                                int ret = pthread_create(&th[i], NULL, make_tree, (void*)&arg[i]);
                                if(ret > 0)
                                {
                                        printf("Unable to create thread %d\n", i);
                                        exit(-1);
                                }
                                
                        }else{ 
                                make_tree((void*) &arg[i]);
                        }
                }
                // Evaluation
                int max_play = 0;
                
                for(i=0; i<7; i++)
                { 
                        if(h==h0)
                        {
                                int ret = pthread_join(th[i], 0);
                                if(ret > 0)
                                {
                                        printf("An error occurred while joining1 thread %d\n", i);
                                        exit(-1);
                                }
                        }

                        // Print diff plays values
                        if(h==h0)
                                printf("son %d, value : %d, played :%d\n", i, (*t)->sons[i]->value, played[i]);

                        // If play is impossible, skip column
                        if(played[i] == -1)
                                continue;


                        int val = (*t)->sons[i]->value;
                                
                        // If main player: maximize, else: minimize.
                        if(g.turn == main_player)
                        {
                                if(val >  (*t)->value ||
                                   val == (*t)->value && eval_play(&g, i, played[i]) > max_play)
                                {
                                        (*t)->value = val;
                                        (*t)->col_value = i;
                                        if(val == (*t)->value)
                                                max_play = eval_play(&g, i, played[i]);
                                }
                        }else{
                                if(val <  (*t)->value ||
                                   val == (*t)->value && eval_play(&g, i, played[i]) < max_play)
                                {
                                        (*t)->value = val;
                                        (*t)->col_value = i;
                                        if(val == (*t)->value)
                                                max_play = eval_play(&g, i, played[i]);
                                }
                        }
                }
        }
        
}

void destruct_tree(tree* t)
{
        if(t->sons[0] == NULL)
                free(&t);
        else{
                int i;
                for(i=0; i<7; i++)
                {
                        destruct_tree(t->sons[i]);
                }
                free(&t);
        }
}
