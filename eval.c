//////////
//  Copyright 2014-2015 Etienne Moutot <e.web@moutot.eu>
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

#include "eval.h"

#include <limits.h>

#define _Pow(x) x*x

int eval_play(game* g, int col, int j)
{
        int player = g->turn;
        int other = 3-(g->turn);

        int nb_other = nb_align(g, other, col, j);
        int nb_player = nb_align(g, player, col, j);

        if(nb_player == 4)
                return _WIN_CST;

        if(nb_other == 4)
                return _WIN_CST*10;

        return _Pow(nb_player) + 10*_Pow(nb_other);
}

int eval(game* g)
{
        int player = g->turn;
        int other = 3-(g->turn);

        int nb_other = 0;
        int nb_player = 0;

        int col;
        int j;
        
        for(col=0; col<7; col++)
        {
                for(j=0; j<6; j++)
                {
                        if(g->t[col][j] == player)
                        {
                                int align = nb_align(g, player, col, j);
                                if(align == 4)
                                        return _WIN_CST;
                                
                                nb_player += _Pow(align);
                        }else if(g->t[col][j] == other)
                        {
                                int align = nb_align(g, other, col, j);
                                if(align == 4)
                                        return -_WIN_CST*10;
                                nb_other += _Pow(align);
                        }
                }
        }

        return nb_player - 10*nb_other;
}


int nb_align(game* g, int player, int col, int j)
{
        int nb=1;
        int act=1;
        // lines
        int i;

        // Lines
        for(i=1; i<4; i++)
        {
                if(col+i>=7)
                        break;
                if(g->t[col+i][j] == player)
                        act++;
                else
                        break;
        }
        if(act>nb)
                nb = act;
        act = 1;

        for(i=-1; i>-4; i--)
        {
                if(col+i<0)
                        break;
                if(g->t[col+i][j] == player)
                        act++;
                else
                        break;
        }
        if(act>nb)
                nb = act;
        act = 1;


        // Columns
        for(i=1; i<4; i++)
        {
                if(j+i>=6)
                        break;
                if(g->t[col][j+i] == player)
                        act++;
                else
                        break;
        }
        if(act>nb)
                nb = act;
        act = 1;

        for(i=-1; i>-4; i--)
        {
                if(j+i<0)
                        break;
                if(g->t[col][j+i] == player)
                        act++;
                else
                        break;
        }
        if(act>nb)
                nb = act;
        act = 1;


        // Diag
        for(i=1; i<4; i++)
        {
                if(j+i>=6 || col+i>=7)
                        break;
                if(g->t[col+i][j+i] == player)
                        act++;
                else
                        break;
        }
        if(act>nb)
                nb = act;
        act = 1;

        for(i=1; i<4; i++)
        {
                if(j-i<0 || col+i>=7)
                        break;
                if(g->t[col+i][j-i] == player)
                        act++;
                else
                        break;
        }
        if(act>nb)
                nb = act;
        act = 1;

        for(i=1; i<4; i++)
        {
                if(j+i>=6 || col-i<0)
                        break;
                if(g->t[col-i][j+i] == player)
                        act++;
                else
                        break;
        }
        if(act>nb)
                nb = act;
        act = 1;

        for(i=1; i<4; i++)
        {
                if(j-i<0 || col-i<0)
                        break;
                if(g->t[col+-i][j-i] == player)
                        act++;
                else
                        break;
        }
        if(act>nb)
                nb = act;
        act = 1;

        return nb;
}
