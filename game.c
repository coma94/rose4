//////////
//  Copyright 2014-2015 Etienne Moutot <e.web@moutot.eu>
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

#include "game.h"

char to_char(int i)
{
        if(i==0)
                return '_';
        else if(i==1)
                return 'X';
        else if(i==2)
                return 'O';
                
}

int to_int(char c)
{
        if(c=='_')
                return 0;
        else if(c=='X')
                return 1;
        else if(c=='O')
                return 2;
}

game decode(FILE* file)
{
        game g;

        char turn;
        fscanf(file, "%c\n", &(turn));
        g.turn = to_int(turn);

        // Change player's turn
        if(g.turn==1)
                g.turn = 2;
        else
                g.turn = 1;

        int i;
        int j;
        char value;
        for(j=5;j>=0;j--)
        {
                for(i=0;i<7;i++)
                {
                        fscanf(file, "%c ", &(value));
                        g.t[i][j] = to_int(value);
                }
        }
        
        return g;
}

void to_file(game g, FILE* file)
{
        fprintf(file, "%c\n", to_char(g.turn));

        int i;
        int j;
        for(j=5;j>=0;j--)
        {
                for(i=0;i<7;i++)
                {
                        fprintf(file, "%c ", to_char(g.t[i][j]));
                }

                fprintf(file, "\n");
        }
}

void print_game(game g)
{
        int i; int j;
        for(j=5;j>=0;j--)
        {
                for(i=0;i<7;i++)
                {
                        printf("%d ", g.t[i][j]);
                }
                printf("\n");
        }
}

int play_allowed(game* g, int col)
{
        if(col<0 || col > 7)
                return -1;
        int j=0;
        while(g->t[col][j] != 0 && j<6)
        {
                j++;
        }
        if(j>=6)
                return -1;
        else
                return j;
}

int play(game* g, int col)
{
        int j = play_allowed(g, col);
        if(j != -1)
        {
                //printf("col: %d, j: %d\n\n", col, j);
                g->t[col][j] = g->turn;

                //printf("nb align %d : %d\n", g->turn, nb_align(g, g->turn, col, j)); 
                return j;
        }
        else
                return -1;
}

