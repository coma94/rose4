//////////
//  Copyright 2014-2015 Etienne Moutot <e.web@moutot.eu>
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <pthread.h>

#include "game.h"
#include "eval.h"
#include "minimax.h"

#define _H_tree 9


int main(int argc, char **argv)
{
        srand(time(0));
        
        ////////////// load
        if(argc < 2)
	{
		printf("usage: %s filename\n", argv[0]);
		return 0;
	}

        game g;

	char filename[32];
	strcpy(filename, argv[1]);
	FILE *file = fopen(filename, "r");
        
	if(file)
	{
                g = decode(file);

                printf("turn: %d\n", g.turn);
                
                int i; int j;
                for(j=5;j>=0;j--)
                {
                        for(i=0;i<7;i++)
                        {
                                printf("%d ", g.t[i][j]);
                        }
                        printf("\n");
                }
                
		fclose(file);
	}else{
                printf("Cannot open %s in r mode", filename);
        }
        
        //////////////
        ////////////// game

        // search for the better column to play
        tree* t = NULL;

        make_tree_arg args;
        args.t = &t;
        args.g = g;
        args.h0 =_H_tree;
        args.h =_H_tree;
        args.main_player = g.turn;
        make_tree((void*) &args);
       
        
        int col=t->col_value;

        play(&g, col);

        printf("Play in : %d\nValue: %d\n", col, t->value);
        
        //////////////
        ////////////// write
        file = fopen(filename, "w");
        if(file)
	{
                
                int i; int j;
                for(j=5;j>=0;j--)
                {
                        for(i=0;i<7;i++)
                        {
                                printf("%d ", g.t[i][j]);
                        }
                        printf("\n");
                }

                to_file(g, file);
                
		fclose(file);
	}else{
                printf("Cannot open %s in w mode", filename);
                }


        // Test tree
        /*tree* t = NULL;
        g.turn = 3 - g.turn; // nvert player: we see the point of veiw of the second who see what can play the firt
        make_tree(&t, g, -1, -1, 1);

        int e = eval_tree(t->sons[3], 1);*/

        //destruct_tree(t);
        return 0;
}
