//////////
//  Copyright 2014-2015 Etienne Moutot <e.web@moutot.eu>
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

#ifndef H_MINIMAX
#define H_MINIMAX

#include "game.h"
#include "eval.h"

typedef struct tree tree;
struct tree
{
        tree* sons[7];

        // Used to evaluate in multithreads
        int value;
        int col_value; // column that corresponds to *value*
};

typedef struct make_tree_arg make_tree_arg;
struct make_tree_arg
{
        tree** t;
        game g;
        int h0;
        int h;
        int main_player;
};
void* make_tree(void* arg);

void destruct_tree(tree* t);

/*
  Eval a tree, using the function sign * eval(). Typically, sign = +/-1
*/
int eval_tree(tree* t, int sign);

/*
  Same but with 1 passed as sign, and "voided" for pthread
*/
void* eval_tree_positive(void* tr);

#endif
