//////////
//  Copyright 2014-2015 Etienne Moutot <e.web@moutot.eu>
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

#ifndef H_EVAL
#define H_EVAL

#include "game.h"

#define _WIN_CST 1000
#define _WIN_CST_STOP _WIN_CST*100

/*
  evaluate a (potential) column to play
*/
int eval_play(game* g, int col, int j);

/*
  evaluate a full game
*/
int eval(game* g);

/*
  How many pions are aligned with the pion posed to the (col, j) position (not look at the rest of the gird)
*/
int nb_align(game* g, int player, int col, int j);

#endif
